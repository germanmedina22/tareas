public class Tarea {
   public static void main(String arg[]) {///main
    System.out.println("Informacion de diferentes Mochilas");
    Escolar es = new Escolar();//instancia la clase Escolar
    es.uso();//metodo uso de la clase Escolar
    es.tamanio();//metodo tamanio de la clase Escolar
    es.informacion();//metodo informacion muestra la demas informacion la clase Escolar

    viaje vi=new viaje();
    vi.uso();//metodo uso de la clase Viaje
    vi.tamanio();//metodo tamanio de la clase Viaje
    vi.informacion();//metodo informacion muestra la demas informacion la clase Viaje
    
    senderismo se=new senderismo();
    se.uso();//metodo uso de la clase senderismo
    se.tamanio();//metodo tamanio de la clase senderismo
    se.informacion();//metodo informacion muestra la demas informacion la clase senderismo
   }
  public static abstract class Mochila {
    ///declaracion de los atributos de la clase Mochila
        private String genero;
        private String color;
        private String marca;
    public Mochila() {
    }
    public Mochila(String genero, String color,String marca) {
        //se asignan los parametros a el constructor de la Mochila
        this.genero= genero;
        this.color= color;
        this.marca = marca;
    }
    public void genero(String g) { //metodo genero ,parametro de String 
            setGenero(g);//genero es igual a la varible del parametro.
    }
    public void color(String c) {//metodo color ,parametro de String 
            setColor(c);//color es igual a la varible del parametro.
    }
    public void marca(String m) {////metodo marca,parametro de String 
            setMarca(m);//marca es igual a la varible del parametro.
    }
    public void informacion() {///metodo informacion , para imprimir la informacion de los metodos de mochila 
        System.out.println("Para genero : " + getGenero());//imprime  el genero de uso de la mochila
        System.out.println("Color: " + getColor());//imprime el color de la mochila
        System.out.println("marca: " + getMarca());//imprime la marca de la mochila
        System.out.println("-----------------");
    }

        /**
         * @return the genero
         */
        public String getGenero() {
            return genero;
        }

        /**
         * @param genero the genero to set
         */
        public void setGenero(String genero) {
            this.genero = genero;
        }

        /**
         * @return the color
         */
        public String getColor() {
            return color;
        }

        /**
         * @param color the color to set
         */
        public void setColor(String color) {
            this.color = color;
        }

        /**
         * @return the marca
         */
        public String getMarca() {
            return marca;
        }

        /**
         * @param marca the marca to set
         */
        public void setMarca(String marca) {
            this.marca = marca;
        }
   }
  public static class Escolar extends Mochila{//subclase
    public Escolar() {
        super("Masculino","Negro","JanSport");//Los parametro de la Mochila
    }
    public String uso() {///Este metodo asignamos el uso 
        String uso= "Uso: Escolar";//se asigna cual es el uso , de tipo String 
        System.out.println(uso);///imprime el uso de la Mochila
        return uso;//retorna el uso de la Mochila
    }
    public String tamanio() {///Este metodo asignamos el tamanio de la moochila
        String tamanio="Mediano";//se asigna cual es el tamanio , de tipo String 
        System.out.println("Tamanio : "+tamanio);//imprimimos el tamanio de la mochila
        return tamanio;//retorna el tamanio de la Mochila
    }
   }
  public static class viaje extends Mochila{//subclase
    public viaje() {
        super("Femenina","Rosado","Totto");//Los parametro de la Mochila
    }
    public String uso() {///Este metodo asignamos el uso 
        String uso= "Uso: Viaje";//se asigna cual es el uso , de tipo String 
        System.out.println(uso);///imprime el uso de la Mochila
        return uso;//retorna el uso de la Mochila
    }
    public String tamanio() {///Este metodo asignamos el tamanio de la moochila
        String tamanio="Mediano";//se asigna cual es el tamanio , de tipo String 
        System.out.println("Tamanio : "+tamanio);//imprimimos el tamanio de la mochila
        return tamanio;//retorna el tamanio de la Mochila  
    }
   }
    public static class senderismo extends Mochila{//subclase
    public senderismo() {
        super("Masculina","Azul","Mountaintop");//Los parametro de la Mochila
    }
    public String uso() {///Este metodo asignamos el uso 
        String uso= "Uso: senderismo";//se asigna cual es el uso , de tipo String 
        System.out.println(uso);///imprime el uso de la Mochila
        return uso;//retorna el uso de la Mochila
    }
    public String tamanio() {///Este metodo asignamos el tamanio de la moochila
        String tamanio="Grande";//se asigna cual es el tamanio , de tipo String 
        System.out.println("Tamanio : "+tamanio);//imprimimos el tamanio de la mochila
        return tamanio;//retorna el tamanio de la Mochila 
    }
   }
}